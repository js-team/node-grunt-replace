## [2.0.2](https://github.com/outaTiME/grunt-replace/compare/2.0.1...2.0.2) (2021-05-03)


### Bug Fixes

* upgrade lodash version to 4.17.21 regarding CWE-78 ([a5f3785](https://github.com/outaTiME/grunt-replace/commit/a5f3785aebfcc8b9b3835c68144e284313651d6a))

## [2.0.2](https://github.com/outaTiME/grunt-replace/compare/2.0.1...2.0.2) (2021-05-03)

